<%-- 
    Document   : index
    Created on : 04-jul-2021, 20:14:19
    Author     : Personal
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
           <h1>Endpoint Api Criptomonedas</h1>
        <h1>Lista Criptomonedas - GET - https://evaluation31.herokuapp.com/api/criptomonedas</h1>
        <h1>Lista Criptomonedas por id - GET - https://evaluation31.herokuapp.com/api/criptomonedas/{id}</h1>
        <h1>Crear Criptomonedas - POST - https://evaluation31.herokuapp.com/api/criptomonedas</h1>
        <h1>Actualizar Criptomonedas - PUT - https://evaluation31.herokuapp.com/api/criptomonedas</h1>
        <h1>Eliminar Criptomonedas - DELETE - https://evaluation31.herokuapp.com/api/criptomonedas</h1>
      
    </body>
</html>
