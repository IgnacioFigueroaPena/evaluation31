/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package root.evaluation31.dao;

import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.Persistence;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import root.evaluation31.dao.exceptions.NonexistentEntityException;
import root.evaluation31.dao.exceptions.PreexistingEntityException;
import root.evaluation31.entity.Criptomonedas;

/**
 *
 * @author Personal
 */
public class CriptomonedasJpaController implements Serializable {

    public CriptomonedasJpaController() {
       
    }
    EntityManagerFactory emf = Persistence.createEntityManagerFactory("criptomonedas");

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Criptomonedas criptomonedas) throws PreexistingEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(criptomonedas);
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findCriptomonedas(criptomonedas.getNombre()) != null) {
                throw new PreexistingEntityException("Criptomonedas " + criptomonedas + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Criptomonedas criptomonedas) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            criptomonedas = em.merge(criptomonedas);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = criptomonedas.getNombre();
                if (findCriptomonedas(id) == null) {
                    throw new NonexistentEntityException("The criptomonedas with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(String id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Criptomonedas criptomonedas;
            try {
                criptomonedas = em.getReference(Criptomonedas.class, id);
                criptomonedas.getNombre();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The criptomonedas with id " + id + " no longer exists.", enfe);
            }
            em.remove(criptomonedas);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Criptomonedas> findCriptomonedasEntities() {
        return findCriptomonedasEntities(true, -1, -1);
    }

    public List<Criptomonedas> findCriptomonedasEntities(int maxResults, int firstResult) {
        return findCriptomonedasEntities(false, maxResults, firstResult);
    }

    private List<Criptomonedas> findCriptomonedasEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Criptomonedas.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Criptomonedas findCriptomonedas(String id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Criptomonedas.class, id);
        } finally {
            em.close();
        }
    }

    public int getCriptomonedasCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Criptomonedas> rt = cq.from(Criptomonedas.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
