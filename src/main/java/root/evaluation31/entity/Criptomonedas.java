/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package root.evaluation31.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Personal
 */
@Entity
@Table(name = "criptomonedas")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Criptomonedas.findAll", query = "SELECT c FROM Criptomonedas c"),
    @NamedQuery(name = "Criptomonedas.findByNombre", query = "SELECT c FROM Criptomonedas c WHERE c.nombre = :nombre"),
    @NamedQuery(name = "Criptomonedas.findBySiglas", query = "SELECT c FROM Criptomonedas c WHERE c.siglas = :siglas"),
    @NamedQuery(name = "Criptomonedas.findByAnocreacion", query = "SELECT c FROM Criptomonedas c WHERE c.anocreacion = :anocreacion"),
    @NamedQuery(name = "Criptomonedas.findByAlgoritmo", query = "SELECT c FROM Criptomonedas c WHERE c.algoritmo = :algoritmo")})
public class Criptomonedas implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "nombre")
    private String nombre;
    @Size(max = 2147483647)
    @Column(name = "siglas")
    private String siglas;
    @Size(max = 2147483647)
    @Column(name = "anocreacion")
    private String anocreacion;
    @Size(max = 2147483647)
    @Column(name = "algoritmo")
    private String algoritmo;

    public Criptomonedas() {
    }

    public Criptomonedas(String nombre) {
        this.nombre = nombre;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getSiglas() {
        return siglas;
    }

    public void setSiglas(String siglas) {
        this.siglas = siglas;
    }

    public String getAnocreacion() {
        return anocreacion;
    }

    public void setAnocreacion(String anocreacion) {
        this.anocreacion = anocreacion;
    }

    public String getAlgoritmo() {
        return algoritmo;
    }

    public void setAlgoritmo(String algoritmo) {
        this.algoritmo = algoritmo;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (nombre != null ? nombre.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Criptomonedas)) {
            return false;
        }
        Criptomonedas other = (Criptomonedas) object;
        if ((this.nombre == null && other.nombre != null) || (this.nombre != null && !this.nombre.equals(other.nombre))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "root.evaluation31.entity.Criptomonedas[ nombre=" + nombre + " ]";
    }
    
}
