/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package root.evaluation31.rest;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import root.evaluation31.dao.CriptomonedasJpaController;
import root.evaluation31.entity.Criptomonedas;


/**
 *
 * @author Personal
 */

@Path("criptomonedas")
public class CriptomonedasRest {

 
@GET
@Produces(MediaType.APPLICATION_JSON)
public Response listarCriptomonedas(){
    
    CriptomonedasJpaController dao=new CriptomonedasJpaController ();
    
    List<Criptomonedas> criptomonedas= dao.findCriptomonedasEntities();
 
    return Response.ok(200).entity(criptomonedas).build();
    
}
@POST
@Produces(MediaType.APPLICATION_JSON)
public Response crear(Criptomonedas criptomonedas){
    
    CriptomonedasJpaController dao=new CriptomonedasJpaController ();
    try {
        dao.create(criptomonedas);
    } catch (Exception ex) {
        Logger.getLogger(CriptomonedasRest.class.getName()).log(Level.SEVERE, null, ex);
    }
    
    return Response.ok(200).entity(criptomonedas).build();
}

@PUT
@Produces(MediaType.APPLICATION_JSON)
public Response actualizar(Criptomonedas criptomonedas){
    CriptomonedasJpaController dao=new CriptomonedasJpaController ();
    
    try {
        dao.edit(criptomonedas);
    } catch (Exception ex) {
    Logger.getLogger(CriptomonedasRest.class.getName()).log(Level.SEVERE, null, ex);
    
     
   
    
}

    
     return Response.ok(200).entity(criptomonedas).build();
    
    }

    
    @DELETE
@Path("/{ideliminar}")
@Produces(MediaType.APPLICATION_JSON)
public Response eliminar(@PathParam("ideliminar") String ideliminar){
    CriptomonedasJpaController dao=new CriptomonedasJpaController();
    try {
        
        
        dao.destroy(ideliminar);
        
        
        
} catch (Exception ex) {
        Logger.getLogger(CriptomonedasRest.class.getName()).log(Level.SEVERE, null, ex);
    }
    return Response.ok("criptomoneda eliminada").build();
}

@GET
@Path("/{idconsulta}")
@Produces(MediaType.APPLICATION_JSON)
public Response consultarPorId(@PathParam("idconsulta") String idconsultar){
System.out.println(""+idconsultar);
CriptomonedasJpaController dao=new CriptomonedasJpaController();
Criptomonedas criptomonedas= dao.findCriptomonedas(idconsultar);

return Response.ok(200).entity(criptomonedas).build();
        
        
    }

}



